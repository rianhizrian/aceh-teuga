<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function test()
    {
        $angka_1 = 1;
        $angka_2 = 5;

        $jumlah = $angka_1 + $angka_2;

        echo $jumlah;
    }

    public function index()
    {
        $semua_kategori = Category::all();

        return view('category.index', compact('semua_kategori'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $kategori = new Category;

        $kategori->nama_kategori = $request->nama_kategori;

        $kategori->save();

        return redirect('/kategori');
    }

    public function edit($id)
    {
        $kategori = Category::find($id);
        return view('category.edit', compact('kategori'));

    }

    public function update(Request $request, $id)
    {
        $kategori = Category::find($id);
        $kategori->nama_kategori = $request["nama_kategori"];

        $kategori->save();

        return redirect('/kategori');
    }
    
    public function destroy($id)
    {
        Category::destroy($id);

        return redirect('/kategori');
    }
}
