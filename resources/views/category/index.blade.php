@extends('admin-new.layouts.app')

@section('title', 'Kategori')

@section('content')
<div class="d-flex mb-3">
    <a href="{{ url('/kategori/create') }}" class="btn btn-primary">Tambah Kategori</a>
</div>
<table class="table table-bordered">
    <tr>
        <th>No.</th>
        <th>Nama Kategori</th>
        <th>Aksi</th>
    </tr>
    @foreach($semua_kategori as $kategori)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td style="text-transform: capitalize;">{{$kategori->nama_kategori}}</td>
        <td>
            <a href="{{ route('edit', ['id' => $kategori->id]) }}" class="btn btn-sm btn-success d-inline mr-2">Edit</a>
            <!-- <a href="{{ route('delete', ['id' => $kategori->id]) }}" class="btn btn-sm btn-block btn-danger">Delete</a> -->
            <form action="{{ route('delete', ['id' => $kategori->id]) }}" method="post" class="d-inline">
                {{ @method_field('DELETE') }}
                {{ csrf_field() }}
                <input type="submit" class="btn btn-sm btn-danger" value="Delete">
            </form>
        </td>
    </tr>
    @endforeach
</table>
@endsection