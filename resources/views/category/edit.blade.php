@extends('admin-new.layouts.app')

@section('title', 'Buat Kategori')

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">Add Category</div>
    </div>
    <div class="card-body">
        <form class="" action="{{ url('/kategori/' . $kategori->id . '/edit') }}" method="post">
            {{ method_field('PUT') }}
            <div class="form-group">
                <label for="nama_kategori">Nama Kategori</label>
                <input type="text" name="nama_kategori" placeholder="isi nama kategori" class="form-control" value="{{$kategori->nama_kategori}}">
            </div>
            {{ csrf_field() }}
            <input type="submit" class="btn btn-success" name="" value="Update Kategori">
        </form>
    </div>
</div>
@endsection