<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin',  function() {
    return view('admin-new.layouts.app');
});

Route::get('/about', function(){
    return view('about');
});

Route::get('/contact', function(){
    return view('contact');
});

Route::get('/test', 'CategoryController@test');
Route::get('/kategori', 'CategoryController@index');
Route::get('/kategori/create', 'CategoryController@create');
Route::post('/kategori/create', 'CategoryController@store');
Route::get('/kategori/{id}/edit', 'CategoryController@edit')->name('edit');
Route::put('/kategori/{id}/edit', 'CategoryController@update');
Route::delete('/kategori/{id}/delete', 'CategoryController@destroy')->name('delete');